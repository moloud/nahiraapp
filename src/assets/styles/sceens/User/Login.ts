import { StyleSheet } from 'react-native';
import dimens from '../../../../res/strings/dimensions';
import colors from '../../../../res/strings/colors';
import fonts from '../../../../res/strings/fonts';

export default StyleSheet.create({
    root: {
        width: '100%',
        height: '100%',
    },
    main: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: dimens._small_margin_padding,
    },
    header: {
        fontSize: dimens._small_font_size,
        fontFamily: fonts.reg,
        color: colors.loginHeader,
        textAlign: 'center',
        width: '100%',
        height: dimens._toolbar,
    },
    imageContainer: {
        marginBottom: dimens._small_margin_padding
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    customeButton: {
        position: 'absolute',
        zIndex: 100,
        bottom: -20,
        alignSelf: 'center',
    }
})