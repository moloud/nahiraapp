import { StyleSheet } from 'react-native';
import dimens from '../../../../res/strings/dimensions';
import colors from '../../../../res/strings/colors';
import fonts from '../../../../res/strings/fonts';

export default StyleSheet.create({
    root: {
        width: '100%',
        height: dimens._button_height,
        borderRadius: dimens._round_button,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.button,
    },
    label: {
        color: colors.buttonText,
        fontSize: dimens._small_font_size,
        fontFamily: fonts.reg,
    }
});
