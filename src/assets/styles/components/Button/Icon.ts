import { StyleSheet } from 'react-native';
import dimens from '../../../../res/strings/dimensions';
import colors from '../../../../res/strings/colors';
import fonts from '../../../../res/strings/fonts';

export default StyleSheet.create({
    root: {
        justifyContent: 'center',
        alignItems: 'center',
        height: dimens._small_button,
        borderRadius: dimens._small_button_radius,
        paddingHorizontal: dimens._small_margin_padding,
        flexDirection: 'row',
    },
    label: {
        fontSize: dimens._xsmall_font_size,
        fontFamily: fonts.light,
        color: colors.buttonText,
        zIndex: 5,
    },
    iconContainer: {
        paddingHorizontal: dimens._xsmall_margin_padding,
    }
})