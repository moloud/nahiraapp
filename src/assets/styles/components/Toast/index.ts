import { StyleSheet } from 'react-native';
import dimens from '../../../../res/strings/dimensions';
import colors from '../../../../res/strings/colors';
import fonts from '../../../../res/strings/fonts';

export default StyleSheet.create({
    modal: {
        backgroundColor: colors.toast,
        borderRadius: 5,
        borderWidth: dimens._border_width,
        borderColor: colors.border,
        width: '90%',
        bottom: dimens._small_margin_padding,
        position: 'absolute',
        zIndex: 200,
        alignSelf: 'center',
        padding: dimens._small_margin_padding,
    },
    text: {
        color: colors.buttonText,
        fontFamily: fonts.reg,
        fontSize: dimens._xsmall_font_size,
        textAlign: 'center'
    }
})