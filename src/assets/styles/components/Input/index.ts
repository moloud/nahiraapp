import { StyleSheet } from 'react-native';
import dimens from '../../../../res/strings/dimensions';
import colors from '../../../../res/strings/colors';
import fonts from '../../../../res/strings/fonts';

export default StyleSheet.create({
    main: {
        width: '100%',
    },
    root: {
        width: '100%',
        height: dimens._button_height,
        borderRadius: dimens._border_Radius,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: colors.cardBackground,
        flexDirection: 'row',
        paddingHorizontal: dimens._xsmall_margin_padding,
        marginVertical: dimens._xsmall_margin_padding,
        elevation: 5,
    },
    activeRoot: {
        width: '100%',
        borderWidth: dimens._border_width,
        borderColor: colors.activeBorder,
        justifyContent: 'space-between',
    },
    seprator: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: '100%',
    },
    line: {
        backgroundColor: colors.line,
        height: '50%',
        width: 1,
        marginHorizontal: dimens._xsmall_margin_padding,
        opacity: 0.4
    },
    textInput: {
        fontFamily: fonts.reg,
        color: colors.inputText,
        fontSize: dimens._xsmall_font_size,
        height: '100%',
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    empty: {
        width: '100%',
        position: 'absolute'
    },
    title: {
        fontFamily: fonts.reg,
        color: colors.text,
        fontSize: dimens._xsmall_font_size,
        textAlign: 'right',
    },
    dot: {
        width: 7,
        height: 7,
        borderRadius: 3.5,
        backgroundColor: colors.required,
        bottom: dimens._xsmall_margin_padding,
    },
    message: {
        fontSize: dimens._xsmall_font_size,
        fontFamily: fonts.reg,
        color: colors.activeText,
        textAlign: 'right',
    },
});
