import { StyleSheet } from 'react-native';
import dimens from '../../../../res/strings/dimensions';
import colors from '../../../../res/strings/colors';
import fonts from '../../../../res/strings/fonts';

export default StyleSheet.create({
    modal: {
        backgroundColor: colors.cardBackground,
        borderWidth: dimens._border_width,
        width: 100,
        height: 100,
        borderRadius: 50,
        alignSelf: 'center',
    },
})