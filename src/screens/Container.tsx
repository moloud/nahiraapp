import React from 'react';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from '@apollo/react-hooks';

const httpLink = createHttpLink({
    uri: 'http://5.9.198.230:4000/graphql'
});

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: httpLink
});

export default (WrapperComponent: React.ReactType) => () => (
    <ApolloProvider client={client}>
        <WrapperComponent />
    </ApolloProvider>
)