import React from 'react';
import { View, ImageBackground, ScrollView, Image, Text } from 'react-native';
import { connect } from 'react-redux';
import { background, profile } from '../../res/strings/images';
import { trans } from '../../helpers';
import { USER_CHECK_EMAIL, LOCAL_LOGIN } from '../../queries/User';
import { changeUser, changeEvent } from '../../action';
import { useMutation } from '@apollo/react-hooks';
import queryAction from '../../queries';
import colors from '../../res/strings/colors';
import styles from '../../assets/styles/sceens/User/Login';

import Camera from '../../components/Icons/Camera';
import Email from '../../components/Icons/Email';
import User from '../../components/Icons/User';
import Input from '../../components/Input';
import IconButton from '../../components/Botton/Icon';
import Button from '../../components/Botton/Simple';
import Loading from '../../components/Loading';
import Toast from '../../components/Toast';

interface loginProps {
    emailOrPhone: string;
    password: string;
    fcm: string;
    changeUser: (value: any) => void;
    changeEvent: (value: any) => void;
}

const Login = ({ emailOrPhone, password, fcm, changeUser, changeEvent }: loginProps) => {
    const onChangeText = (name: string) => (value: string) => changeUser({ [name]: value });
    const { data } = queryAction({ email: emailOrPhone }, USER_CHECK_EMAIL);
    const [localLogin, result] = useMutation(LOCAL_LOGIN)
    const submit = () => localLogin({ variables: { emailOrPhoneNumber: emailOrPhone, password, fcm } })
        .then(response => changeEvent({ toast: true, messege: `accessToken = ${response.data.localLogin.token.accessToken}` }))
        .catch(e => changeEvent({ toast: true, messege: e.message }))
    return (
        <>
            <Loading open={result.loading} />

            <ImageBackground source={background} style={styles.root}>
                <ScrollView contentContainerStyle={styles.main}>
                    <Text style={styles.header}>{trans('LOGIN_HEADER')}</Text>
                    <View style={styles.imageContainer}>
                        <Image source={profile} style={styles.image} />
                        <IconButton
                            onPress={() => { }}
                            label={trans('CHOOSE_PHOTO')}
                            color={colors.blueButton}
                            customeStyle={styles.customeButton}
                            Icon={Camera}
                        />
                    </View>
                    <Input
                        onChangeText={onChangeText('emailOrPhone')}
                        placeholder={trans('USER_NAME')}
                        value={emailOrPhone}
                        Icon={User}
                        title={trans('USER_NAME')}
                        message={data && data.userCheckEmail.message}
                        required
                    />
                    <Input
                        value={password}
                        onChangeText={onChangeText('password')}
                        placeholder={'***********'}
                        Icon={Email}
                        title={trans('PASSWORD')}
                        required
                    />
                    <Button label={trans('ENTER')} onPress={submit} />
                </ScrollView>
            </ImageBackground>
            <Toast />
        </>
    );
}

const mapStateToProps = state => ({
    emailOrPhone: state.user.emailOrPhone,
    password: state.user.password,
    fcm: state.user.fcm,
});

const dispatchToProps = dispatch => ({
    changeUser: payload => dispatch(changeUser(payload)),
    changeEvent: payload => dispatch(changeEvent(payload)),
})


export default connect(mapStateToProps, dispatchToProps)(Login);