import * as React from 'react';
import { SvgCss } from 'react-native-svg';

interface propType {
    background: string;
    tick: string;
}
export default ({ background, tick }: propType) => <SvgCss
    xml={
        `<svg id="Group_2836" data-name="Group 2836" xmlns="http://www.w3.org/2000/svg" width="18.565" height="18.565" viewBox="0 0 18.565 18.565">
        <defs>
          <style>
            .cls-1 {
              fill: ${background};
            }
      
            .cls-2 {
              fill: ${tick};
            }
          </style>
        </defs>
        <circle id="Ellipse_111" data-name="Ellipse 111" class="cls-1" cx="9.283" cy="9.283" r="9.283" transform="translate(0)"/>
        <path id="Path_2367" data-name="Path 2367" class="cls-2" d="M192.967,250.174a.958.958,0,0,0-1.355,0l-4.279,4.279-1.645-1.645a.958.958,0,0,0-1.355,1.355l2.323,2.323a.958.958,0,0,0,1.355,0l4.956-4.956A.958.958,0,0,0,192.967,250.174Z" transform="translate(-179.411 -244.091)"/>
      </svg>
      `
    } />;