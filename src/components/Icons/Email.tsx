import * as React from 'react';
import { SvgCss } from 'react-native-svg';

interface propType {
  color: string;
}
export default ({ color }: propType) => <SvgCss
  xml={
    `<svg xmlns="http://www.w3.org/2000/svg" width="17.355" height="17.355" viewBox="0 0 17.355 17.355">
        <defs>
          <style>
            .cls-1 {
              fill: ${color};
            }
          </style>
        </defs>
        <path id="email" class="cls-1" d="M14.814,2.542A8.678,8.678,0,0,0,2.542,14.814a8.686,8.686,0,0,0,9.514,1.859l-.418-.989a7.605,7.605,0,1,1,4.644-7.007c0,1.837-1.128,2.673-2.177,2.673s-2.177-.836-2.177-2.673a3.254,3.254,0,1,0-.652,1.95,3.353,3.353,0,0,0,.574.827,3.107,3.107,0,0,0,4.509,0,3.931,3.931,0,0,0,1-2.777A8.621,8.621,0,0,0,14.814,2.542ZM8.678,10.854a2.177,2.177,0,1,1,2.177-2.177A2.179,2.179,0,0,1,8.678,10.854Z" transform="translate(0 0)"/>
      </svg>
      `
  } />;