import * as React from 'react';
import { SvgCss } from 'react-native-svg';

interface propType {
  color: string;
}

export default ({ color }: propType) => <SvgCss
  xml={
    `<svg id="Group_2828" data-name="Group 2828" xmlns="http://www.w3.org/2000/svg" width="22.339" height="24.051" viewBox="0 0 22.339 24.051">
        <defs>
          <style>
            .cls-1 {
              fill: ${color};
            }
          </style>
        </defs>
        <path id="Path_2392" data-name="Path 2392" class="cls-1" d="M48.884,14.707V10.492a.543.543,0,0,0-.241-.448A1.433,1.433,0,0,1,48,8.852V4.478a3.4,3.4,0,0,1,3.4-3.4h.94a3.4,3.4,0,0,1,3.4,3.4V8.852a1.433,1.433,0,0,1-.64,1.192.529.529,0,0,0-.241.448v4.215a.735.735,0,0,0,.41.662,33.9,33.9,0,0,1,6.118,3.925,1.567,1.567,0,0,1,.574,1.219v3a.541.541,0,0,0,1.083,0v-3a2.667,2.667,0,0,0-.968-2.056,35.349,35.349,0,0,0-6.134-3.964V10.76a2.5,2.5,0,0,0,.88-1.908V4.478A4.483,4.483,0,0,0,52.34,0H51.4a4.483,4.483,0,0,0-4.478,4.478V8.852a2.5,2.5,0,0,0,.88,1.908v3.734a35.348,35.348,0,0,0-6.134,3.964,2.633,2.633,0,0,0-.968,2.056v3a.541.541,0,0,0,1.083,0v-3a1.567,1.567,0,0,1,.574-1.219,34.231,34.231,0,0,1,6.118-3.925A.735.735,0,0,0,48.884,14.707Z" transform="translate(-40.7 0)"/>
      </svg>
      `
  } />;