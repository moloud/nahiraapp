import React from 'react';
import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';
import { View } from 'react-native';
import { loading } from '../../res/strings/animations'
import colors from '../../res/strings/colors';
import styles from '../../assets/styles/components/Loading';

interface loadingProp {
    open: boolean;
}
const Loading = ({ open }: loadingProp) => (
    <Modal
        isVisible={open}
        backdropColor={colors.shade}
        backdropOpacity={0.8}
        animationIn="fadeIn"
        animationOut="fadeOut"
    >
        <View style={styles.modal}>
            <LottieView source={loading} autoPlay loop />
        </View>
    </Modal>
);

export default Loading;
