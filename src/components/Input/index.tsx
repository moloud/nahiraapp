import React from 'react';
import { View, TextInput, Text } from 'react-native';
import colors from '../../res/strings/colors';
import styles from '../../assets/styles/components/Input';

import Ok from '../Icons/Ok';

interface inputProp {
	onChangeText: (value: any) => void;
	value: string;
	placeholder: string;
	title: string;
	required: boolean | undefined;
	Icon: React.FC<any>;
	message: string | undefined;
}

const InputIcon = ({ Icon, onChangeText, value, placeholder, title, required, message }: inputProp) => (
	<View style={styles.main}>
		<Text style={styles.title}>{title}</Text>
		{!message && <View style={styles.root}>
			<TextInput
				style={[styles.textInput, styles.empty]}
				placeholderTextColor={colors.placeholder}
				{...{ onChangeText, value, placeholder }}
			/>
			<View style={styles.line} />
			<Icon color={colors.inputIcon} />
			{required && <View style={styles.dot} />}
		</View>
		}
		{message && (
			<>
				<View style={[styles.root, styles.activeRoot]}>
					<View style={styles.seprator}>
						{required && <View style={styles.dot} />}
						<Icon color={colors.inputIcon} />
						<View style={styles.line} />
					</View>
					<TextInput
						style={styles.textInput}
						placeholderTextColor={colors.placeholder}
						{...{ onChangeText, value, placeholder }}
					/>
					<View style={styles.seprator}>
						<View style={styles.line} />
						<Ok tick={colors.tick} background={colors.ok} />
					</View>

				</View>
				<Text style={styles.message}>{message}</Text>
			</>
		)}

	</View>
)

export default InputIcon;
