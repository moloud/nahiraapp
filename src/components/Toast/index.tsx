import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { clearEvent } from '../../action';
import styles from '../../assets/styles/components/Toast';

interface toastProps {
    toast: boolean;
    message: string;
    clearEvent: () => void
}
const Toast = ({ toast, messege, clearEvent }: toastProps) => {
    useEffect(() => {
        setTimeout(() => clearEvent(), 1000);
    }, [toast]);
    return toast ? (
        <View style={styles.modal}>
            <Text style={styles.text}>{messege}</Text>
        </View>
    ) : null;
}

const mapStateToProps = state => ({
    toast: state.event.toast,
    messege: state.event.messege,
});

const dispatchToProps = dispatch => ({
    clearEvent: () => dispatch(clearEvent()),
});

export default connect(mapStateToProps, dispatchToProps)(Toast);
