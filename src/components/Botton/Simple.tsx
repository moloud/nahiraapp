import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import styles from '../../assets/styles/components/Button/Simple';

interface buttonProps {
    label: string;
    onPress: () => void;
};

const SimpleButton = ({ label, onPress }: buttonProps) => (
    <TouchableOpacity style={styles.root} {...{ onPress }}>
        <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
);

export default SimpleButton;
