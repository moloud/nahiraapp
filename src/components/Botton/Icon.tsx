import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import colors from '../../res/strings/colors';
import styles from '../../assets/styles/components/Button/Icon';

const IconButton = ({ Icon, label, color, customeStyle, onPress }) => (
    <TouchableOpacity {...{ onPress }}>
        <View style={[styles.root, { backgroundColor: color }, customeStyle || {}]}>
            <Text style={styles.label}>{label}</Text>
            <View style={styles.iconContainer}>
                <Icon color={colors.buttonIcon} />
            </View>
        </View>
    </TouchableOpacity>
);
export default IconButton;