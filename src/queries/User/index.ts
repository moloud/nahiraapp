import gql from 'graphql-tag';

export const USER_CHECK_EMAIL = gql`
query($email: String!) {
  userCheckEmail(email:$email){
      exist
      message
    }
}
`;

export const LOCAL_LOGIN = gql`
mutation login($emailOrPhoneNumber: String!, $password: String!, $fcm: String!){
  localLogin(loginInput:{emailOrPhoneNumber: $emailOrPhoneNumber,password: $password,fcm: $fcm }){
      _id
    token {
			refreshToken
      accessToken
  	}
	}
}
`;
