import { useQuery } from '@apollo/react-hooks';

export default (variables: any, queris: any) => {
    const { loading, error, data } = useQuery(queris, {
        variables: { ...variables }
    });
    return { loading, error, data }
}
