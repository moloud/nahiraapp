// USER
export const clearUser = () => ({ type: 'CLEAR_USER' });
export const changeUser = payload => ({ type: 'CHANGE_USER', payload });
export const sendUserLogin = payload => ({ type: 'SEND_USER_LOGIN', payload });
export const sendUserLoginRequest = () => ({ type: 'SEND_USER_LOGIN_REQUEST' });
export const sendUserLoginSuccess = payload => ({ type: 'SEND_USER_LOGIN_SUCCESS', payload });
export const sendUserLoginFail = payload => ({ type: 'SEND_USER_LOGIN_FAIL', payload });

// EVENT
export const clearEvent = () => ({ type: 'CLEAR_EVENT' });
export const changeEvent = payload => ({ type: 'CHANGE_EVENT', payload });
