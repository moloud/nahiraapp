import actionGenerator from '../../helpers/actionGenrator';

export const user = {
    CLEAR_USER: 'CLEAR_USER',
    CHANGE_USER: 'CHANGE_USER',
    ...actionGenerator('SEND_USER_LOGIN'),
};
