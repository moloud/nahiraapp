const actionGenrator = (actionName: string) => ({
    [actionName]: actionName,
    [`${actionName}_REQUEST`]: `${actionName}_REQUEST`,
    [`${actionName}_SUCCESS`]: `${actionName}_SUCCESS`,
    [`${actionName}_FAIL`]: `${actionName}_FAIL`,
});

export default actionGenrator;