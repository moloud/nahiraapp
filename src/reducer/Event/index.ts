import { event as eventType } from '../../action/types/Event';

const initialState = {
    loading: false,
    toast: false,
    messege: '',
};

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case eventType.CLEAR_EVENT:
            return initialState;
        case eventType.CHANGE_EVENT:
            return { ...state, ...action.payload };
        default:
            return state;
    };
};
