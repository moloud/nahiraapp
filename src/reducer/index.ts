import { combineReducers } from 'redux';

import user from './User';
import event from './Event';

export default combineReducers({
    user,
    event,
})

