import { user as userType } from '../../action/types/User';

const initialState = {
    emailOrPhone: '09364436650',
    password: '123123123',
    fcm: 'RANDOM_STRING',
};

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case userType.CLEAR_USER:
            return initialState;
        case userType.CHANGE_USER:
            return { ...state, ...action.payload };
        default:
            return state;
    };
};
