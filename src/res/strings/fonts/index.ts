export default {
    reg: 'IRANSansMobile',
    ultralight: 'IRANSansMobile_Ultralight',
    medium: 'IRANSansMobile_Medium',
    light: 'IRANSansMobile_Light',
    bold: 'IRANSansMobile_Bold',
    black: 'IRANSansMobile_Black',
};
