export default {
	// PALETTE
	GhostWhite: '#F7F7F9',
	IrishBlue: '#03BECD',
	PortGore: '#3D4672',
	Wistful: '#A6A8C3',
	VeryLightGray: '#CDCDCD',
	White: '#FFFFFF',
	WhiteSmoke: '#F3F3F3',
	SunsetOrange: '#FF5454',
	MoodyBlue: '#7F74C7',


	get background() {
		return this.GhostWhite;
	},

	get buttonText() {
		return this.White;
	},

	get buttonIcon() {
		return this.White;
	},

	get blueButton() {
		return this.IrishBlue;
	},

	get loginHeader() {
		return this.PortGore;
	},

	get border() {
		return this.WhiteSmoke;
	},

	get cardBackground() {
		return this.White;
	},

	get line() {
		return this.PortGore;
	},

	get inputText() {
		return this.PortGore;
	},

	get placeholder() {
		return this.Wistful;
	},

	get inputIcon() {
		return this.Wistful;
	},

	get text() {
		return this.Wistful;
	},

	get required() {
		return this.SunsetOrange;
	},

	get button() {
		return this.MoodyBlue;
	},

	get activeBorder() {
		return this.IrishBlue;
	},

	get activeText() {
		return this.IrishBlue;
	},

	get ok() {
		return this.IrishBlue;
	},

	get tick() {
		return this.White;
	},

	get toast() {
		return this.SunsetOrange;
	},
};
