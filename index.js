import React from 'react';
import { Navigation } from "react-native-navigation";
import { Provider } from 'react-redux';
import store from './src/redux/store';
import Container from './src/screens/Container'
import Login from "./src/screens/User/Login";

Navigation.registerComponentWithRedux('Login', () => Container(Login), Provider, store);


Navigation.events().registerAppLaunchedListener(() => {
	Navigation.setRoot({
		root: {
			component: {
				name: "Login"
			}
		}
	});
});